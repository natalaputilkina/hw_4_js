// 1. Запитайте у користувача два числа.
// Перевірте, чи є кожне з введених значень числом.
// Якщо ні, то запитуйте у користувача нове занчення до тих пір, поки воно не буде числом.
// Виведіть на екран всі цілі числа від меншого до більшого за допомогою циклу for.


let a;
do {
    a = parseInt(prompt('Enter number one: '));
}while(isNaN(a));

let b;
do {
    b = parseInt(prompt('Enter number two: '));
}while(isNaN(b));

let min;
let max;

if (a > b){
    max = a;
    min = b;
}
else {
    min = a;
    max = b;
}

for(let i = min; i <= max; i++){
 
    console.log(i);
    
}
